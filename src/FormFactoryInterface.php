<?php

namespace Procontext\Form;

use Psr\Http\Message\ServerRequestInterface;

interface FormFactoryInterface
{
    public function make(ServerRequestInterface $request): Form;
}