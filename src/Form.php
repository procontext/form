<?php

namespace Procontext\Form;

use Procontext\Form\Exception\ValidationException;
use \ReflectionClass;
use \ReflectionProperty;

abstract class Form
{
    public $url;
    public $ip;
    public $user_agent;
    public $tool_name;
    public $utm_source = '';
    public $utm_medium = '';
    public $utm_campaign = '';
    public $utm_content = '';
    public $utm_term = '';
    public $debug = false;

    protected $properties;
    protected $errors;

    public function __construct(array $formData)
    {
        $formData = $this->preload($formData);
        $reflection = new ReflectionClass(get_called_class());
        $this->properties = array_column($reflection->getProperties(ReflectionProperty::IS_PUBLIC), 'name');
        foreach ($this->properties as $property) {
            if (isset($formData[$property])) {
                if($property == 'url') {
                    $queryParams = [];
                    parse_str(
                        parse_url(urldecode($formData[$property]), PHP_URL_QUERY),
                        $queryParams
                    );
                    foreach ($queryParams as $param => $value) {
                        switch ($param) {
                            case 'utm_source':
                                $this->utm_source = $value;
                                break;
                            case 'utm_medium':
                                $this->utm_medium = $value;
                                break;
                            case 'utm_campaign':
                                $this->utm_campaign = $value;
                                break;
                            case 'utm_content':
                                $this->utm_content = $value;
                                break;
                            case 'utm_term':
                                $this->utm_term = $value;
                                break;
                        }
                    }
                    $this->url = url_parse($formData[$property]);

                    $envHost =  parse_url(env('URL'), PHP_URL_HOST) . parse_url(env('URL'), PHP_URL_PATH);
                    $envHost = rtrim($envHost, '/');

                    $formHost =  parse_url($formData[$property], PHP_URL_HOST) . parse_url($formData[$property], PHP_URL_PATH);
                    $formHost = rtrim($formHost, '/');

                    if($envHost !== $formHost) {
                        $this->errors[] = "Поле url не совпадает с url указанным в настройках";
                    }

                } else {
                    $this->{$property} = $formData[$property];
                }
            } elseif ($property == 'tool_name') {
                $this->tool_name = 'Форма на сайте ' . env('URL');
            } elseif(
                !in_array($property, ['utm_source', 'utm_medium', 'utm_campaign', 'utm_content', 'utm_term'])
                && !isset($this->{$property})
            ) {
                $this->errors[] = "Поле " . $property . " не найдено";
            }
        }

        if($this->errors) {
            throw new ValidationException($this->errors);
        }
    }

    protected function preload(array $formData): array
    {
        return $formData;
    }

    public function export(): array
    {
        $formData = [];
        foreach ($this->properties as $property) {
            $formData[$property] = $this->{$property};
        }
        return $formData;
    }

    public function errors(): array
    {
        return $this->errors;
    }
}
