<?php

namespace Procontext\Form\Exception;

use Throwable;

class ValidationException extends FormException
{
    protected $messageBag;

    public function __construct($messageBag = [], $message = 'Ошибка валидации формы', $code = 400, Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
        $this->messageBag = $messageBag;
    }

    public function getMessageBag(): array
    {
        return $this->messageBag;
    }
}

